$file_data = Get-Content .\checkup_conf.txt

For ($i = 0; $i -lt $file_data.Length; $i++) {
    If ( $null -ne $file_data[$i]  ) { 
        If ( $file_data[$i].Split("=")[0] -eq "IVTFS1"  ) { 
            $IVTFS1 = $file_data[$i].Split("=")[1]
        }
        elseif ($file_data[$i].Split("=")[0] -eq "IVTFS2") {
            $IVTFS2 = $file_data[$i].Split("=")[1]
        }
        elseif ($file_data[$i].Split("=")[0] -eq "FB") {
            $IVTFB = $file_data[$i].Split("=")[1]

        }
        elseif ($file_data[$i].Split("=")[0] -eq "UI") {
            $IVTFUI = $file_data[$i].Split("=")[1]
        }
        elseif ($file_data[$i].Split("=")[0] -eq "PORT") {
            $ListPort = $file_data[$i].Split("=")[1].Split(",")
            
        }
    }
}

$OriginalProgressPreference = $Global:ProgressPreference
$Global:ProgressPreference = 'SilentlyContinue'
$myArray = $IVTFS1, $IVTFS2, $IVTFUI, $IVTFB
## Test de ping
Write-Host "Test ping"

For ($i = 0; $i -lt $myArray.Length; $i++) {
    If ( $null -ne $myArray[$i]  ) { 
        Write-Host "Ping" $myArray[$i]
        $Output = Test-Connection $myArray[$i] -Quiet
        Write-Host "Result :" $Output

    }
}

For ($i = 0; $i -lt $ListPort.Length; $i++) {

    If ( $null -ne $ListPort[$i]  ) {
        Write-Host "Test port " $ListPort[$i].Split(":")[1] "on"  (Get-Variable $ListPort[$i].Split(":")[0]).Value
        $Output = Test-NetConnection -ComputerName (Get-Variable $ListPort[$i].Split(":")[0]).Value -Port $ListPort[$i].Split(":")[1] -InformationLevel "Quiet" -WarningAction SilentlyContinue
        Write-Host "Result :" $Output

    }

}

$Global:ProgressPreference = $OriginalProgressPreference
